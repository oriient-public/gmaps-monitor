# Overview
This is a demonstration of how to connect to Oriient's real-time server and receive real-time updates about a user's location.
This will display the map of a selected floor from the user's building (provided using the buildingId variable, see ahead), which we've already positioned on the world map.
You should see the map rendered in the correct position and proportions.
Once there is a user that has started positioning, there should be seen a marker representing his device in real-time on the map.

# How to run
1.  Clone the repository
2.  Go into src/App.js and change the following variables:
    * buildingId
    * spaceId
    * apiKey
    * If you intend to integrate with our PreProd environment, make sure that following variables are set correctly:
        * restApiUrl = 'https://apipp.oriient.me'
        * ipsServerUrl = 'wss://ipspp.oriient.me'
    * If you intend to intergrate with our Production environment:
        * restApiUrl = 'https://api.oriient.me'
        * ipsServerUrl = 'wss://ips.oriient.me'
3.  Install dependencies: `npm install`
4.  Run: `npm start`

# How to work with Oriient's real-time server
You can find the WebSocket-related code in src/Monitor.js.
All the instructions in this section are implemented in the various methods there.

In order to work with Oriient's real-time server, you need to do the following:
1.  Then, you need to connect to our websocket.
2.  Once connected, you need to send a login message using the following format:
```js
{
    "type": 'login',
    "apiKey": <apiKey>,
    "userId": <SomeUserID>,
    "deviceId": <RandomUniqueIdentifier>
}
```
3.  After that, you should receive a message with `type: loginAck`. At that point, send a message to monitor a specific map, using the following format:
```js
{
    "type": 'monitorMapV2',
    "mapId": <mapId>
}
```
4.  At this point, you will start receiving message of with `type: positionBroadcast` or `type: positionUpdate`, which will include the real-time position of the device. The message will be in the following format:
```js
{
    "type": "positionBroadcast",
    "position": {
        "t": "2019-07-17T06:07:29.131Z",
        "d": 200,
        "x": 23.65509033,
        "y": 29.07297134,
        "z": 24.5,
        "lng": 34.79394913,
        "lat": 32.06893158,
        "elv": 24.5,
        "azm": 285.1001579,
        "h": [
            0.01920019756,
            0.9998156592,
            0
        ],
        "v": 0.7,
        "u": 17.37623405,
        "f": 6
    },
    "mapId": <mapId>,
    "floorId": <floorId>,
    "spaceId": <spaceId>,
    "ipsClientId": <clientId>,
    "sessionId": <sessionId>,
    "oriientInternals": {
        "sysSmpl": 780
    },
    "rtPositionsFormat": "v2",
    "clientId": "emulatorPackageTest",
    "monitorPositionsFormat": "v2"
}
```

# Position message properties explanation
* t - UTC timestamp
* d - duration of the position. t + d = next position t
* x - x coordinates in Oriient’s building coordinates system 
* y - y coordinates in Oriient’s building coordinates system 
* z - z coordinates in Oriient’s building coordinates system (height)
* lng - longitude
* lat - latitude
* elv - elevation, also known as altitude
* azm - azimuth of the position in angles where 0 is the north
* h - heading of the position as vector.  In Oriient’s building coordinates system.
* v - velocity of movement [m/sec]
* u - Uncertainty of the position. How accurate in meters this position is (standard deviation)
* f - floor index
* mapId - the map that the position is on
* floorId - the floor that the position is on
* spaceId - the user space
* ipsClientId - unique identifier for the client. Consistent between sessions from the same device
* sessionId - session identifier
* rtPositionsFormat - version format of real time positions for mobile clients
* monitorPositionsFormat - version format of real time positions for monitor clients
* clientId - the clientId name that was used during login request

