import React, {useCallback, useEffect, useState} from "react";
import randomColor from 'randomcolor';
import GMaps from "./GMaps";
import Monitor from './Monitor'

const colorList = [
    'blue',
    'green',
    'red',
    'brown',
    'black',
    'gray',
    'magenta',
    'cyan',
    'gold',
    'hotpink',
    'lightsalmon',
    'lime',
    'navy',
    'purple',
    'steelblue',
    'maroon',
    'orchid',
    'orangered',
    'darkgreen',
    'midnightblue',
].reverse();

// Fill these to start working
const buildingId = '<Insert Building ID here>';
const spaceId = '<Insert Space ID here>';
const apiKey = '<Insert API key here>';

// This will integrate with our PreProd environment. To integrate with Production, change apipp.oriient.me to api.oriient.me
const restApiUrl = 'https://apipp.oriient.me';

// This will integrate with our PreProd environment. To integrate with Production, change ipspp.oriient.me to ips.oriient.me
const ipsServerUrl = 'wss://ipspp.oriient.me';

const monitor = new Monitor(ipsServerUrl);

const App = () => {
    const [building, setBuilding] = useState(null);
    const [floor, setFloor] = useState(null);
    const [map, setMap] = useState(null);
    const [markers, setMarkers] = useState({});
    const [selectedFloorIndex, setSelectedFloorIndex] = useState(null);


    const handleFloorChange = async floorIndex => {
        const floor = building.floors.find(f => f.floorIndex === floorIndex);
        setFloor(floor);
        setSelectedFloorIndex(floorIndex);

        // This will integrate with our PreProd environment. To integrate with Production, change apipp.oriient.me to api.oriient.me
        const res = await fetch(`${restApiUrl}/v3/buildings/${buildingId}/floors/${floor.floorId}/maps/${floor.maps[0].mapId}?spaceId=${spaceId}&api_key=${apiKey}`);
        const map = await res.json();
        setMap(map);
        setMarkers({});
    };

    const getSessionColor = () => {
        if (!colorList.length) {
            // fill it with more colors other than the basic ones
            for (let i = 0; i < 10; i++) {
                colorList.push(randomColor({luminosity: 'bright'}));
            }
        }
        return colorList.pop();
    };

    const handlePosition = useCallback(message => {
        const {sessionId, clientId, position} = message;

        setMarkers(prevMarkers => {
            if (prevMarkers[sessionId]) {
                clearTimeout(prevMarkers[sessionId].agingTimeout);
            }

            return {
                ...prevMarkers,
                [sessionId]: {
                    ...position,
                    lat: position.lat,
                    lng: position.lng,
                    uncertainty: position.u,
                    heading: position.azm,
                    color: prevMarkers[sessionId] ? prevMarkers[sessionId].color : getSessionColor(),
                    clientId,
                    sessionId,
                    agingTimeout: setTimeout(() => {
                        setMarkers(prevMarkers => {
                            delete prevMarkers[sessionId];
                            return prevMarkers;
                        })
                    }, 10000)
                }
            }
        });
    }, []);

    const fetchBuilding = useCallback(async () => {
        const response = await fetch(`${restApiUrl}/v4/buildings/${buildingId}?spaceId=${spaceId}&api_key=${apiKey}`);
        const building = await response.json();

        setBuilding(building);
        setSelectedFloorIndex(building.floors[0].floorIndex)
    }, []);

    useEffect(() => {
        fetchBuilding();
    }, [fetchBuilding]);

    useEffect(() => {
        if (map) {
            monitor.connectToWebSocket(map.mapId, apiKey, handlePosition);
        }
    }, [map, handlePosition]);

    if (!building) {
        return null
    }

    return (
        <GMaps building={building} markers={markers} floor={floor} map={map}
               onFloorChange={handleFloorChange}
               selectedFloorIndex={selectedFloorIndex}
        />
    )
};

export default App;
