export default class Monitor {
    constructor(ipsServerUrl) {
        this.ipsServerUrl = ipsServerUrl;
        this.webSocket = null;
        this.shouldReconnect = false;
        this.onPosition = () => {
        };
        this.mapId = null;
    }

    connectToWebSocket = (mapId = this.mapId, apiKey = this.apiKey, onPosition = this.onPosition) => {
        this.mapId = mapId;
        this.apiKey = apiKey;
        this.onPosition = onPosition;

        if (!this.webSocket || (this.webSocket && this.webSocket.readyState === WebSocket.CLOSED)) {
            this.webSocket = new WebSocket(this.ipsServerUrl);
            this.webSocket.onopen = this.handleOpenWebSocket;
            this.webSocket.onclose = this.handleCloseWebSocket;
            this.webSocket.onerror = this.handleErrorWebSocket;
            this.webSocket.onmessage = this.handleMessage;
        } else {
            this.shouldReconnect = true;
            this.disconnectFromWebSocket();
        }
    };

    disconnectFromWebSocket = () => {
        if (this.webSocket) {
            this.webSocket.close();
        }
    };

    handleOpenWebSocket = () => {
        const deviceId = Math.floor(Math.random() * 1000000 + 1);
        const {apiKey} = this;

        // Send login message to web socket
        this.webSocket.send(
            JSON.stringify({
                type: 'login',
                apiKey,
                userId: 'monitor',
                deviceId: 'monitorDemo' + deviceId,
            })
        );
    };

    handleCloseWebSocket = () => {
        if (this.shouldReconnect) {
            this.connectToWebSocket();
            this.shouldReconnect = false;
        }
    };

    handleErrorWebSocket = () => {
        this.connectToWebSocket();
    };

    handleMessage = event => {
        const message = JSON.parse(event.data);
        const {type, error, reason} = message;

        if (error) {
            console.log('ERROR,', error, reason);
            return;
        }

        switch (type) {
            case 'loginAck':
                this.webSocket.send(
                    JSON.stringify({
                        type: 'monitorMapV2',
                        mapId: this.mapId,
                    })
                );
                break;

            case 'respPlmsNearLocation':
            case 'respSearchPlms':
            case 'respPlmByName':
            case 'respPlmByNameId':
            case 'respTopLayerImage':
            case 'logoutResp':
                break;

            case 'positionBroadcast':
            case 'positionUpdate':
                this.onPosition(message);
                break;

            default:
                break;
        }

    }
}
