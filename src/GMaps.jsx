import React from 'react';
import styled from 'styled-components';
import {
    LoadScript,
    GoogleMap,
    Marker,
    OverlayView,
} from '@react-google-maps/api';
import {List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import {ReactComponent as oriientMarker} from './oriientMarker.svg';

const PositionMarker = styled(oriientMarker)`
    #Oriient_AppIcon {
        fill: ${props => props.color};
    }

    width: 50px;
    height: 50px;
`;

const MapImageWrapper = styled.div`
    transform: translateY(-100%) rotate(${props => props.rotation}deg);
    transform-origin: bottom left;
`;

const MapImage = styled.img`
    opacity: 0.5;
    width: ${props => props.imageWidth}px;
    height: ${props => props.imageHeight}px;
    transform: translate(${props => props.offset.x}px, -${props => props.offset.y}px);
    transform-origin: bottom left;
    transition: all 200ms ease;
`;

const FloorList = styled(List)`
    background-color: white;
    display: inline-flex;
    flex-direction: column;
    position: absolute !important;
    bottom: 0;
    margin: 20px !important;
    border: 1px solid coral;
`;

const FloorListItem = styled(ListItem)`
    cursor: pointer;
`;

const SessionColorBox = styled.div`
    width: 10px;
    height: 10px;
    background-color: ${props => props.color};
`;

const Legend = styled(List)`
    display: inline-flex;
    flex-direction: column;
    background-color: white;
    border-radius: 3px;
    margin: 20px;
    border: 1px solid coral;
`;

// Keep this array outside the component class to avoid re-passing it as a new array
// in the prop every render which will re-fetch the libraries
const libraries = ['places', 'visualization'];

class GMaps extends React.Component {
    mapImageRef = React.createRef();

    state = {
        isExpanded: true,
        showPositions: true,
        showHeatMap: false,
        mapOffset: {x: 0, y: 0},
        imageHeight: 0,
        imageWidth: 0,
        isMapLoaded: false,
        center: {
            lat: this.props.building.buildingOrigin.latitude,
            lng: this.props.building.buildingOrigin.longitude,
        },
        selectedFloorIndex: this.props.building.floors[0].floorIndex,
    };

    componentDidMount() {
        this.props.onFloorChange(this.state.selectedFloorIndex)
    }

    handleMapLoad = map => {
        this.map = map;
        this.setState({isMapLoaded: true});
    };

    handleMapClick = () => {
        const {isExpanded} = this.state;
        if (!isExpanded) {
            this.setState({isExpanded: true});
        }
    };

    handleZoomChange = () => {
        const {building, map} = this.props;
        const {isMapLoaded} = this.state;

        if (isMapLoaded && this.mapImageRef.current) {
            const metersPerPixel = 156543.03392 * Math.cos(building.buildingOrigin.latitude * Math.PI / 180) / Math.pow(2, this.map.getZoom());
            const heightInMeters = this.mapImageRef.current.naturalHeight / map.pixelToMeter;
            const widthInMeters = this.mapImageRef.current.naturalWidth / map.pixelToMeter;

            this.setState({
                imageHeight: heightInMeters / metersPerPixel,
                imageWidth: widthInMeters / metersPerPixel,
                mapOffset: {x: map.mapOffset.x / metersPerPixel, y: map.mapOffset.y / metersPerPixel}
            })
        }
    };

    render() {
        const {building, map, markers, onFloorChange, selectedFloorIndex} = this.props;
        const {isExpanded, mapOffset, imageHeight, imageWidth, isMapLoaded, center} = this.state;
        const LatLng = isMapLoaded ? window.google.maps.LatLng : null;

        return (
            <LoadScript googleMapsApiKey={'AIzaSyCx-8MpHBe63RJ5bq3dpaM5F-_MbccgRF8'} libraries={libraries}>
                <GoogleMap
                    onLoad={this.handleMapLoad}
                    onClick={this.handleMapClick}
                    onZoomChanged={this.handleZoomChange}
                    mapContainerStyle={{
                        height: isExpanded ? '100%' : 150,
                        width: isExpanded ? '100%' : 150,
                        top: 0,
                        position: 'absolute',
                        overflow: 'hidden',
                        left: 0,
                        transition: 'all 200ms ease',
                    }}
                    zoom={18}
                    center={center}
                    options={{
                        streetViewControl: false,
                        fullscreenControl: false,
                        mapTypeControl: false,
                    }}
                >
                    <FloorList>
                        {building.floors.sort((f1, f2) => f2.floorIndex - f1.floorIndex).map(f => (
                            <FloorListItem key={f.floorIndex} selected={selectedFloorIndex === f.floorIndex}
                                           onClick={() => onFloorChange(f.floorIndex)}>
                                <ListItemText>{f.floorIndex}</ListItemText>
                            </FloorListItem>
                        ))}
                    </FloorList>

                    {isMapLoaded && map && <OverlayView
                        position={new LatLng(building.buildingOrigin.latitude, building.buildingOrigin.longitude)}
                        mapPaneName={OverlayView.OVERLAY_LAYER}
                    >
                        <MapImageWrapper rotation={building.buildingToEnuRotation} offset={mapOffset}>
                            <MapImage alt={'map'} src={map.mapUrl} ref={this.mapImageRef}
                                      imageWidth={imageWidth} imageHeight={imageHeight}
                                      rotation={building.buildingToEnuRotation} offset={mapOffset}
                                      onLoad={this.handleZoomChange}
                            />
                        </MapImageWrapper>
                    </OverlayView>}

                    {isMapLoaded && markers && Object.values(markers).map(m =>
                        <OverlayView key={m.sessionId} mapPaneName={OverlayView.OVERLAY_LAYER}
                                     position={new LatLng(m.lat, m.lng)}>
                            <PositionMarker color={m.color}
                                            style={{transform: `translate(-50%, -50%) rotate(${m.heading}deg)`}}/>
                        </OverlayView>
                    )}

                    {isMapLoaded && markers &&
                    <Legend dense>
                        {Object.values(markers).map(m => (
                            <ListItem key={m.sessionId}>
                                <ListItemIcon>
                                    <SessionColorBox color={m.color}/>
                                </ListItemIcon>
                                <ListItemText>{m.clientId}</ListItemText>
                            </ListItem>
                        ))}
                    </Legend>
                    }

                    {isMapLoaded && <Marker position={new LatLng(
                        building.buildingOrigin.latitude,
                        building.buildingOrigin.longitude
                    )}/>}
                </GoogleMap>
            </LoadScript>
        );
    }
}

export default GMaps;
